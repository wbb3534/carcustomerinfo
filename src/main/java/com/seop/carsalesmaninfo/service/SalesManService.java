package com.seop.carsalesmaninfo.service;

import com.seop.carsalesmaninfo.entity.SalesMan;
import com.seop.carsalesmaninfo.exception.CMissingDataException;
import com.seop.carsalesmaninfo.model.*;
import com.seop.carsalesmaninfo.repository.SalesManRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalesManService {
    private final SalesManRepository salesManRepository;

    public void setSalesManInfo(SalesManInfoRequest request) {
        SalesMan addData = new SalesMan.SalesManBuilder(request).build();

        salesManRepository.save(addData);
    }

    public ListResult<SalesManInfoItem> getSalesMansInfo() {
        List<SalesMan> originData = salesManRepository.findAll();
        List<SalesManInfoItem> result = new LinkedList<>();

        originData.forEach(item -> result.add(new SalesManInfoItem.SalesManInfoItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public SalesManInfoResponse getSalesManInfo(long id) {
        SalesMan origin = salesManRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new SalesManInfoResponse.SalesManInfoResponseBuilder(origin).build();
    }

    public void putSalesManInfo(long id, SalesManInfoUpdateRequest request) {
        SalesMan salesMan = salesManRepository.findById(id).orElseThrow(CMissingDataException::new);
        salesMan.putSalesManInfo(request);

        salesManRepository.save(salesMan);

    }
}
