package com.seop.carsalesmaninfo.repository;

import com.seop.carsalesmaninfo.entity.SalesMan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalesManRepository extends JpaRepository<SalesMan, Long> {
}
