package com.seop.carsalesmaninfo.controller;

import com.seop.carsalesmaninfo.model.*;
import com.seop.carsalesmaninfo.service.ResponseService;
import com.seop.carsalesmaninfo.service.SalesManService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "영업사원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sales-man")
public class SalesManController {
    private final SalesManService salesManService;

    @ApiOperation(value = "영업사원 정보 등록")
    @PostMapping("/new")
    public CommonResult setSalesManInfo(@RequestBody @Valid SalesManInfoRequest request) {
        salesManService.setSalesManInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "영업사원 정보 리스트")
    @GetMapping("/list")
    public ListResult<SalesManInfoItem> getSalesMansInfo() {
        return ResponseService.getListResult(salesManService.getSalesMansInfo(), true);
    }

    @ApiOperation(value = "영업사원 정보 단수")
    @GetMapping("/data/{id}")
    public SingleResult<SalesManInfoResponse> getSalesManInfo(@PathVariable long id) {
        return ResponseService.getSingleResult(salesManService.getSalesManInfo(id));
    }
    @ApiOperation(value = "영업사원 정보 수정")
    @PutMapping("/info/{id}")
    public CommonResult putSalesManInfo(@PathVariable long id, @RequestBody @Valid SalesManInfoUpdateRequest request) {
        salesManService.putSalesManInfo(id, request);

        return ResponseService.getSuccessResult();
    }
}
