package com.seop.carsalesmaninfo.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
