package com.seop.carsalesmaninfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarSalesManInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarSalesManInfoApplication.class, args);
    }

}
