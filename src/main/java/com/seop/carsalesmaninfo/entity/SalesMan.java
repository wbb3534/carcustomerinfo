package com.seop.carsalesmaninfo.entity;

import com.seop.carsalesmaninfo.enums.WorkDepartment;
import com.seop.carsalesmaninfo.interfaces.CommonModelBuilder;
import com.seop.carsalesmaninfo.model.SalesManInfoRequest;
import com.seop.carsalesmaninfo.model.SalesManInfoUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SalesMan {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "영업사원명")
    @Column(nullable = false, length = 20)
    private String salesManName;
    @ApiModelProperty(value = "영업사원 번호")
    @Column(nullable = false, length = 13)
    private String salesManPhone;
    @ApiModelProperty(value = "근무 부서")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private WorkDepartment workDepartment;
    @ApiModelProperty(value = "입사일")
    @Column(nullable = false)
    private LocalDate joinCompanyDate;

    public void putSalesManInfo(SalesManInfoUpdateRequest request) {
        this.salesManName = request.getSalesManName();
        this.salesManPhone = request.getSalesManPhone();
        this.workDepartment = request.getWorkDepartment();
    }
    private SalesMan(SalesManBuilder builder) {
        this.salesManName = builder.salesManName;
        this.salesManPhone = builder.salesManPhone;
        this.workDepartment = builder.workDepartment;
        this.joinCompanyDate = builder.joinCompanyDate;
    }
    public static class SalesManBuilder implements CommonModelBuilder<SalesMan> {
        private final String salesManName;
        private final String salesManPhone;
        private final WorkDepartment workDepartment;
        private final LocalDate joinCompanyDate;

        public SalesManBuilder(SalesManInfoRequest request) {
            this.salesManName = request.getSalesManName();
            this.salesManPhone = request.getSalesManPhone();
            this.workDepartment = request.getWorkDepartment();
            this.joinCompanyDate = request.getJoinCompanyDate();
        }
        @Override
        public SalesMan build() {
            return new SalesMan(this);
        }
    }

}
